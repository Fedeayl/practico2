#########################################################
################ TALLER R ESPACIAL ######################
#########################################################

# Limpiar el entorno # 
rm(list = ls())

######### 1) CARGAMOS ARCHIVOS #############
# Instalar paquetes #
# install.packages("sp")
# pkgs <- c("RgoogleMaps","foreign","plyr","shapefiles","maptools","rgeos","geometry","splancs",
#           "cclust","leaflet","rgdal","dplyr","tidyr","ggmap","ggplot2","tmap","RColorBrewer",
#           "classInt","devtools","shiny"))
# install.packages(pkgs)

# Activar los paquetes de funciones con library() o require()
library(sp)
library(rgdal)
require(sf)


# O se puede hacer una lista con las bibliotecas, y luego aplicarle la función library()
#x <- c("devtools","plyr","shapefiles","rgdal","dplyr","ggmap","RColorBrewer","maptools","rgeos",
#       "deldir","geometry","splancs","cclust","classInt","leaflet","shiny")
#lapply(x, library, character.only = TRUE)

#carga una función desde una sintaxis de R
source("voronoi.R")

# Cargar Objetos propios de R #
# En este caso un dataframe ya generado con las coordenadas y el dato de ICC de una poblacion objetivo
# Los archivos Rdata pueden contener cualquier tipo de objetos, aqui solo un dataframe
load("pob.Rdata") 

# Ver su estructura #
View(pob) #abre una nueva pestaña
str(pob)

# y vemos un resumen de sus datos
summary(pob)

# Cargar una base en .csv conviertiéndola en un data.frame#
# En este caso es una tabla con la modalidad y los recursos humanos de los socats
datos <- read.csv("mod_rrhh_socat.csv", sep = "\t")

# Vemos su estructura #
str(datos)

#también podemos hacer una tabla
table(datos$rh)
#o verlo en un gráfico
barplot(table(datos$rh),col = "blue")

# Generar un objeto espacial a partir de coordenadas #
# Convertimos el dataframe (las columnas con coordenadas) de la población objetivo a SpatialPoints
pob_sp <- SpatialPoints(pob[,1:2],proj4string = CRS("+init=epsg:32721"))

# Vemos su estructura #
str(pob_sp)

# y vemos un resumen de sus datos
summary(pob_sp)

#Convertimos los SpatialPoints a SpatialPointsDataFrame
pob_spdf <- SpatialPointsDataFrame(pob_sp, pob)

# Vemos su estructura #
str(pob_spdf)

# y vemos un resumen de sus datos
summary(pob_spdf)

# y también podemos abrir su @data
View(pob_spdf@data)

# guardamos el objeto espacial que creamos en formato .shp (por si es necesario)
writeOGR(pob_spdf, dsn = getwd(),layer = 'poblacion', driver = 'ESRI Shapefile', overwrite = TRUE)

# Cargar archivos .shp #
# Aqui cargamos los municipios de Montevideo para usar de marco
municipios <- readOGR(dsn = 'municipios.shp')
# municipios <- maptools::readShapeSpatial("municipios.shp", proj4string = CRS("+init=epsg:32721"))
# municipios <- maptools::readShapePoly("municipios.shp", proj4string = CRS("+init=epsg:32721")) 

plot(municipios)

# Aqui cargamos los centros SOCAT de Montevideo
socats <- rgdal::readOGR(dsn = "centros_socat_mvd.shp")

# y vemos un resumen de sus datos
summary(socats)

plot(pob_spdf, col = "blue", add = T)
plot(socats, col = "red", pch = 16, add = T)

# Aqui cargamos los centros SOCAT de Montevideo
AT <- readOGR(dsn = 'at_mvd.shp')

# y vemos un resumen de sus datos
summary(AT)

plot(AT, col = "transparent", border = "green", add = T)

# Ahora hagamos algo con la librería SF
barrios = st_read("barrios.gpkg")

# Vemos su estructura #
str(barrios)

# y vemos un resumen de sus datos
summary(barrios)

# Guardar los objetos del entorno, por si me cansé y quiero retomar mañana #  
save.image()  
# save(list = ls(all.names = TRUE), file = "respaldo.RData") # Sería lo mismo pero nombrando el archivo
